<?php
/**
 * @file
 * Administrative functionality for the sparkmenus module.
 */

/**
 * Admin form.
 */
function sparkmenus_settings() {
  $form = array();

  // Load the variables.
  $sparkmenus_vocabulary1 = intval(variable_get('sparkmenus_vocabulary1', 0));
  $sparkmenus_vocabulary2 = intval(variable_get('sparkmenus_vocabulary2', 0));
  $sparkmenus_blocks = intval(variable_get('sparkmenus_blocks', 3));

  // Build an array of vocabularies.
  $vocabs = array();
  foreach (taxonomy_get_vocabularies() as $vocab) {
    $vocabs[$vocab->vid] = $vocab->name;
  }
  asort($vocabs);

  // Build an array of content types.
  $content_types = array();
  foreach (node_get_types() as $type) {
    $content_types[$type->type] = $type->name;
  }
  asort($content_types);

  // Nest the main settings in a fieldset.
  $form['categories'] = array(
    '#collapsible' => FALSE,
    '#description' => t('Select the vocabulary and main categories to use.'),
    '#title' => t('Category Selection'),
    '#type' => 'fieldset',
  );

  // Control how many menus are available.
  $form['categories']['sparkmenus_blocks'] = array(
    '#default_value' => $sparkmenus_blocks,
    '#description' => t('The number of blocks that are available. Setting to <em>Disable</em> will hide all blocks.'),
    '#options' => array(0 => t('Disable'), 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5),
    '#title' => t('Number of menu blocks'),
    '#type' => 'select',
  );

  // Primary vocabulary.
  $form['categories']['sparkmenus_vocabulary1'] = array(
    '#default_value' => $sparkmenus_vocabulary1,
    '#description' => t('The categories/terms that are used are picked from this vocabulary, so it must be assigned first.'),
    '#options' => $vocabs,
    '#title' => t('Primary vocabulary'),
    '#type' => 'select',
  );
  // Secondary vocabulary.
  $form['categories']['sparkmenus_vocabulary2'] = array(
    '#default_value' => $sparkmenus_vocabulary2,
    '#description' => t('The categories/terms that are also listed.'),
    '#options' => $vocabs,
    '#title' => t('Secondary vocabulary'),
    '#type' => 'select',
  );

  // Which to show first?
  $form['categories']['sparkmenus_show_first'] = array(
    '#default_value' => intval(variable_get('sparkmenus_show_first', 1)),
    '#description' => t('In the blocks that are created there are two columns of links, this controls which list is displayed on the left.'),
    '#options' => array(1 => 'First vocabulary', 2 => 'Second vocabulary'),
    '#title' => t('Which is shown first?'),
    '#type' => 'radios',
  );

  // Content type selector.
  $form['content_types'] = array(
    '#collapsible' => FALSE,
    '#description' => t('Select the content types that the selection is limited to.'),
    '#title' => t('Content Type Selection'),
    '#type' => 'fieldset',
  );
  $form['content_types']['sparkmenus_content_types'] = array(
    '#type' => 'checkboxes',
    '#options' => $content_types,
    '#default_value' => variable_get('sparkmenus_content_types', array()),
    '#title' => '',
  );


  // Only show the term selectors if the primary vocabulary was selected.
  if (!empty($sparkmenus_vocabulary1)) {
    // Build a nested array of terms.
    $terms = array();
    foreach (taxonomy_get_tree($sparkmenus_vocabulary1) as $term) {
      $terms[$term->tid] = $term->name;
      // Nest the term names.
      if ($term->depth > 0) {
        $terms[$term->tid] = str_repeat('   ', $term->depth) . '-> ' . $terms[$term->tid];
      }
    }

    // Add the fields for the term selectors.
    $form['terms'] = array(
      '#collapsible' => FALSE,
      '#description' => t('Select the parent categories that will be used to build the menus.  These must have sub-terms / sub-categories otherwise it won\'t work correctly<br />Additionally, a <a href="!link">jQuery Selector</a> must be provided to match where on the page the block should be placed.', array('!link', 'http://api.jquery.com/')),
      '#title' => t('Block selection'),
      '#type' => 'fieldset',
    );

    // Add a selector for the desired number of blocks.
    for ($block = 1; $block <= $sparkmenus_blocks; $block++) {
      $category_var = 'sparkmenus_category'. $block;
      $selector_var = 'sparkmenus_selector'. $block;
      $form['terms'][$category_var] = array(
        '#default_value' => intval(variable_get($category_var, '')),
        '#options' => $terms,
        '#required' => TRUE,
        '#title' => t('Menu #!menu', array('!menu' => $block)),
        '#type' => 'select',
      );
      $form['terms'][$selector_var] = array(
        '#default_value' => variable_get($selector_var, "#primary-menu ul li:eq({$block})"),
        '#required' => TRUE,
        '#title' => t('jQuery Selector #!menu', array('!menu' => $block)),
        '#type' => 'textfield',
      );
    }
  }

  return system_settings_form($form);
}
