<?php
/**
 * @file
 * Some functionality separated into its own file.
 */

/**
 * Obtain a set of terms that match a vocabulary and also have content tagged
 * with a given term.
 *
 * @param $vid Vocabulary ID for the vocabular to limit the search.
 * @param $term Term ID that all items must also have.
 */
function sparkmenus_get_terms($term, $content_types, $vid = 0) {
  $results = array();

  // Placeholders for compiling the array of content types.
  $content_type_placeholders = db_placeholders($content_types, 'varchar');

  // Get the tree of terms underneath this primary term, including the term
  // itself.
  $terms = array($term->tid);
  foreach (taxonomy_get_tree($term->vid, $term->tid) as $term) {
    $terms[] = $term->tid;
  }
  $terms_placeholders = db_placeholders($terms);

  // Only bother proceeding if something was found.
  if (!empty($terms)) {
    // Decide which query to use:
    if (!empty($vid)) {
      // Query:
      // Terms from the $vid with active nodes that are also tagged with $tid,
      // restricted to $content_types.
      $query = db_query("SELECT DISTINCT
          td1.tid,
          td1.name
        FROM
          {term_data} td1
          INNER JOIN {term_node} tn1
            ON td1.tid = tn1.tid
          INNER JOIN {term_node} tn2
            ON tn1.nid = tn2.nid
          INNER JOIN {node} n
            ON tn1.nid = n.nid
            AND tn1.vid = n.vid
        WHERE
          td1.vid = %d
          AND tn2.tid IN ({$terms_placeholders})
          AND n.type IN ({$content_type_placeholders})
          AND n.status = 1
        ORDER BY
          td1.name", array_merge(array($vid), $terms, $content_types));
    }
    else {
      // Query:
      // All sub-terms of the selected term that have active nodes, restricted
      // to $content_types.
      $query = db_query("SELECT DISTINCT
          td.tid,
          td.name
        FROM
          {term_data} td
          INNER JOIN {term_node} tn
            ON td.tid = tn.tid
          INNER JOIN {node} n
            ON tn.nid = n.nid
            AND tn.vid = n.vid
        WHERE
          td.tid IN ({$terms_placeholders})
          AND n.type IN ({$content_type_placeholders})
          AND n.status = 1
        ORDER BY
          td.name", array_merge($terms, $content_types));
    }

    // Compile the array of terms.
    while ($term = db_fetch_array($query)) {
      $results[] = array(
        'tid' => $term['tid'],
        'name' => $term['name'],
        'name_clean' => sparkmenus_cleanstring($term['name']),
        'link' => l($term['name'], 'taxonomy/term/'. $term['tid']),
      );
    }
  }

  // Return a compiled array of term data.
  return $results;
}

/**
 * A wrapper for many string cleaning functions provided by other modules.
 */
function sparkmenus_cleanstring($string) {
  // PathAuto is the preferred option, but only use some of its functionality.
  if (module_exists('pathauto')) {
    // Load all of the Pathauto include files.
    _pathauto_include();

    // Optionally remove accents and transliterate
    if (variable_get('pathauto_transliterate', FALSE)) {
      static $translations;

      if (!isset($translations)) {
        $translations = FALSE;
        if ($file = _pathauto_get_i18n_file()) {
          $translations = parse_ini_file($file);
        }
      }

      if (!empty($translations)) {
        $string = strtr($string, $translations);
      }
    }

    // Replace or drop punctuation based on user settings
    $separator = variable_get('pathauto_separator', '-');
    $punctuation = pathauto_punctuation_chars();
    foreach ($punctuation as $name => $details) {
      $action = variable_get('pathauto_punctuation_'. $name, 0);
      // 2 is the action for "do nothing" with the punctuation
      if ($action != 2) {
        // Slightly tricky inline if which either replaces with the separator or nothing
        $string = str_replace($details['value'], ($action ? $separator : ''), $string);
      }
    }

    // Reduce strings to letters and numbers
    if (variable_get('pathauto_reduce_ascii', FALSE)) {
      $pattern = '/[^a-zA-Z0-9\/]+/';
      $string = preg_replace($pattern, $separator, $string);
    }

    // Always replace whitespace with the separator.
    $string = preg_replace('/\s+/', $separator, $string);

    // Trim duplicates and remove trailing and leading separators.
    $string = _pathauto_clean_separators($string);

    // Optionally convert to lower case.
    $string = drupal_strtolower($string);
  }
  
  return $string;
}

/**
 * Very simple function for storing the compiled output for later, to be
 * loaded later during template_preprocess_page().
 */
function sparkmenus_store_output($output = '') {
  static $storage = '';

  if (!empty($output)) {
    $storage .= $output;
  }

  return $storage;
}
