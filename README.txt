
SparkMenus
**********
A pseudo-menu that creates two-column pseudo-blocks containing links to term
pages from two related vocabularies.

A typical use case would be for a store website that had their products
broken into a few main categories with lists of subcategories and a second
vocabulary for storing brands, this allows listing of all of the sub-
categories and the brands within each main category in one focused block.

It was originally written to provide an advanced menu structure for the
FidoDogTreats.com site without having to build "MegaMenus" or something that
involved a good deal of manual intervention.  A key goal was to allow it to
be completely automated after the initial setup, that term changes would be
handled without issue.

This module currently only supports Drupal 6.

What's in a name?
****************
While building the module a natural question arose: what to call it?  After
trying to summarize the functionality in a few words created eloquent options
like "Two Column Term-Based Pseudo-Menu Pseudo-Blocks", "twocoltermmenublock",
"termcolumnmenublock" or "NotaMenu", so we took the high road and opted for
blatant self promotion =)

Sponsorship
***********
Development was sponsored by Bluespark Labs (http://www.bluesparklabs.com/)
and Fido Dog Treats (http://fidodogtreats.com/).

Author
******
Written by Damien McKenna (http://drupal.org/user/108450).
