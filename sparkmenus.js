/**
 * @file
 * Custom JS for SparkMenus.
 */

// Only proceed if the proper settings have been defined.
$(document).ready(function() {
  if (Drupal != undefined && Drupal.jsEnabled != undefined && Drupal.jsEnabled) {
    if (Drupal.settings != undefined && Drupal.settings.sparkmenus != undefined) {
      // Loop over each defined block.
      jQuery.each(Drupal.settings.sparkmenus, function(js_index, js_value) {
        // Reposition the blocks.
        var block_offset = $(js_value).offset();
        var block_left = block_offset.left;
        var block_top = block_offset.top + $(js_value).attr('offsetHeight');
        var sparkmenus_block = 'div.sparkmenus:eq('+js_index+')';

        // I'd just use $('').offset(offset) but that requires jQuery 1.4.
        $(sparkmenus_block).css('left', block_left);
        $(sparkmenus_block).css('top', block_top);

        // Show & hide hover actions.
        $(js_value).hover(function(e) {
          // Stop the timer.
          clearTimeout(this.sfTimer);
          // Show the block.
          $(sparkmenus_block).show();
        }, function(e) {
          // Start the timer to display it.  Thank you admin_menu :)
          this.sfTimer = setTimeout(function() {
            // if ($(sparkmenus_block).css('display') == 'none'){
              $(sparkmenus_block).hide();
            // }
          }, 400);
        });

        // Make the menus remain displayed.
        $(sparkmenus_block).mouseover(function(e) {
          $(js_value).mouseover();
        });
        $(sparkmenus_block).mouseout(function(e) {
          $(js_value).mouseout();
        });
      });
    }
  }
});
