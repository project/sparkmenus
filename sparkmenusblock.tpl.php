<?php
/**
 * @file
 * Template for displaying the full SparkMenus block.
 *
 * Available variables:
 * $rows - a nested array containing all of the data to display.
 * $rows[$column] - where $column is either 'left' or 'right'.
 * $rows[$column]['tid'] - the term's ID / tid.
 * $rows[$column]['name'] - the name of this item's term.
 * $rows[$column]['name_clean'] - a trimmed version of the term name, suitable
 *   for use as a CSS class, etc.
 * $rows[$column]['link'] - the full link to this item's term.
 * $rows[$column]['row'] - the number of the current row, starting from 1.
 * $rows[$column]['column'] - the string 'left' or 'right'.
 * $rows[$column]['total'] - the total number of rows.
 * $rows[$column]['item'] - the final processed output for this item.
 * $category - the term object for this block, contains the usual term fields.
 * $category->name_clean - a trimmed version of the term name, suitable for
 *   use as a CSS class, etc.
 */
?>
<div class="sparkmenus sparkmenus-<?php print $category->name_clean; ?>" style="display:none;">
  <?php foreach ($rows as $column => $data): ?>
    <ul class="sparkmenus-<?php print $column; ?>">
      <?php foreach ($data as $row): ?>
        <?php print $row['item']; ?>
      <?php endforeach; ?>
    </ul>
  <?php endforeach; ?>
  <div style="clear:both;"></div>
</div>
