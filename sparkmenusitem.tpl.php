<?php
/**
 * @file
 * Template for displaying a single item in the SparkMenus block.
 *
 * Available variables:
 * $row - an array containing the data for this cell.
 * $row['tid'] - the term's tid.
 * $row['name'] - the term's name.
 * $row['name_clean'] - a trimmed version of the term name, suitable for use
 *   as a CSS class, etc.
 * $row['link'] - a link to the term page.
 * $row['column'] - either 'left' or 'right'.
 * $row['row'] - the current row number within this column.
 * $row['total'] - the total number of rows in this column.
 */
?>
<li class="sparkmenus-item sparkmenus-item-<?php print $row['row']; ?>">
  <?php print $row['link']; ?>
</li>
